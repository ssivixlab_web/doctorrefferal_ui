import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { API_END_POINT_URL, API_URL } from '../constants/constants';

@Injectable({
  providedIn: 'root',
})
export class APIService {
  private apiURL = API_END_POINT_URL;

  private searchedData$ = new BehaviorSubject(null);

  constructor(private httpClient: HttpClient) {}

  public setSearchData(data) {
    this.searchedData$.next(data);
  }

  public populateForm() {
    return this.searchedData$;
  }

  public searchDoctorByName(payload): Observable<any> {
    return this.httpClient.post(`${this.apiURL}${API_URL.GET_DOCTOR}`, payload);
  }

  public refferPatientToDoctor(payload, id): Observable<any> {
    const HttpUploadOptions = {
      headers: new HttpHeaders(),
    };
    let url = `${this.apiURL}${API_URL.DOCTOR_REFER}`;
    if (id) {
      url = `${url}/${id}`;
    }
    return this.httpClient.put(`${url}`, payload, HttpUploadOptions);
  }

  public searchRefferal(id): Observable<any> {
    return this.httpClient.get(`${this.apiURL}${API_URL.DOCTOR_REFER}/${id}`);
  }
}
