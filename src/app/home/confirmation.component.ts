import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  template: ` <h2 class="mb-0" mat-dialog-title>Confirm</h2>
    <div mat-dialog-content>
      <div>
        Hi, <b>Dr. {{ data.referredBy }}</b> You are referring a Patient to
        <b>Dr. {{ data.referredTo }}</b
        >.
      </div>

      <p>Below are the Patient Details:</p>
      <mat-divider></mat-divider>

      <div><span class="text-muted">Name:</span> {{ data.patientName }}</div>
      <div>
        <span class="text-muted">NRIC/FIN/Passport:</span> {{ data.referralId }}
      </div>
      <div>
        <span class="text-muted">Contact Info:</span>
        {{ data.patientEmail }} &nbsp; {{ data.patientMobileNum }}
      </div>
    </div>
    <div class="mt-2" mat-dialog-actions>
      <button mat-flat-button color="accent" (click)="clickEvent(true)">
        Continue
      </button>
      <button mat-stroked-button color="warn" (click)="clickEvent(false)">
        Cancel
      </button>
    </div>`,
  encapsulation: ViewEncapsulation.None,
})
export class ConfirmationModalComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ConfirmationModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  clickEvent(item) {
    this.dialogRef.close(item);
  }
}
