import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SearchModalComponent } from '../search-modal/search-modal.component';
import { ConfirmationModalComponent } from './confirmation.component';
import { APIService } from './home.service';
@Component({
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  refferalPatientFg: FormGroup;
  inputFileUpload: File[] = [];
  message = null;
  private referredByObj = null;
  private referredToObj = null;
  step = 0;
  private modalConfig: MatDialogConfig = {
    closeOnNavigation: true,
    disableClose: true,
  };

  constructor(
    private _fb: FormBuilder,
    private searApi: APIService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.createForm();
    this.searApi.populateForm().subscribe((res) => {
      if (res) {
        this.referredByObj = res.referredBy_Info;
        this.referredByObj._id = res.referredBy;
        this.referredToObj = res.referredTo_Info;
        this.referredToObj._id = res.referredTo;

        const formPayload = {
          patientEmail: res.patientEmail,
          patientMobileNum: res.patientMobileNum,
          patientName: res.patientName,
          referralId: res.referralId,
          referredBy: res.referredBy_Info.name,
          referredTo: res.referredTo_Info.name,
          _id: res._id,
        };
        this.refferalPatientFg.patchValue(formPayload);
      }
    });
  }

  private setMessage(className: string, message: string) {
    this.message = {
      clsNme: className,
      msg: message,
    };
  }

  private clearMsg() {
    this.message = null;
  }

  private createForm(): void {
    this.refferalPatientFg = this._fb.group({
      referredBy: new FormControl({ value: '', disabled: true }),
      referredTo: new FormControl({ value: '', disabled: true }),
      referralId: [''],
      patientName: [''],
      patientEmail: [''],
      patientMobileNum: [''],
      _id: [''],
    });
  }

  private resetForm(): void {
    this.refferalPatientFg.reset();
    this.inputFileUpload = [];
    this.referredByObj = null;
    this.referredToObj = null;
    this.step = 0;
    setTimeout(() => {
      this.clearMsg();
    }, 5000);
  }

  searchDoc(search): void {
    const dialogRef = this.dialog.open(SearchModalComponent, {
      ...this.modalConfig,
      data: search,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (result.ev === 'from') {
          this.refferalPatientFg.get('referredBy').setValue(result.docObj.name);
          this.referredByObj = result.docObj;
        }
        if (result.ev === 'to') {
          this.refferalPatientFg.get('referredTo').setValue(result.docObj.name);
          this.referredToObj = result.docObj;
        }
      }
    });
  }

  setStep(index: number): void {
    this.step = index;
  }

  nextStep(): void {
    this.step++;
  }

  prevStep(): void {
    this.step--;
  }

  openFile(fileupload: FileList): void {
    if (fileupload.length > 0) {
      for (let i = 0; i < fileupload.length; i++) {
        this.inputFileUpload.push(fileupload.item(i));
      }
    }
  }

  private validateForm(formData): boolean {
    this.clearMsg();
    let valid: boolean = true;
    if (formData.referredBy === '' || formData.referredTo === '') {
      this.setMessage('text-danger', 'Referral From & To Doctor are required');
      valid = false;
      this.setStep(0);
    } else if (formData.patientName === '') {
      this.setMessage('text-danger', 'Patient Name is required');
      valid = false;
      this.setStep(1);
    } else if (formData.referralId === '') {
      this.setMessage(
        'text-danger',
        'Please provide Patient NRIC/FIN/Passport No'
      );
      valid = false;
      this.setStep(1);
    } else if (formData.patientMobileNum === '') {
      this.setMessage('text-danger', 'Please provide Patient Mobile Number');
      valid = false;
      this.setStep(1);
    }
    return valid;
  }

  submitRefferal(payload): void {
    if (this.validateForm(payload)) {
      let submitFormObj = {
        ...payload,
        referredBy: this.referredByObj._id,
        referredTo: this.referredToObj._id,
      };
      this.confirmBeforeSubmit(payload, submitFormObj);
    }
  }

  removeFile(file): void {
    this.inputFileUpload = this.inputFileUpload.filter(
      (item) => item.name !== file.name
    );
  }

  private confirmBeforeSubmit(formData, submitFormObj): void {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      ...this.modalConfig,
      data: formData,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.sendData(submitFormObj);
      }
    });
  }

  private sendData(submitFormData): void {
    const id = submitFormData._id;
    if (submitFormData._id) {
      delete submitFormData._id;
    }
    const formData = new FormData();
    this.inputFileUpload.forEach((file: File) => {
      formData.append('file', file, file.name);
    });
    formData.append('data', JSON.stringify(submitFormData));
    this.searApi.refferPatientToDoctor(formData, id).subscribe((res) => {
      if (res && res.status) {
        this.setMessage('text-success', res.message);
        this.resetForm();
      }
    });
  }
}
