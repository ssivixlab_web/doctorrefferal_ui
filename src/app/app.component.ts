import { Component, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { APIService } from './home/home.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'docRefApp';
  errorMsg = {
    required: 'This field is required',
    invalid: 'This is invalid'
  };
  searchFc: FormControl = new FormControl('', Validators.required);
  constructor(private apiSer: APIService) { }
  getError(FormControl: string): string {
    const fc: AbstractControl = this.searchFc;
    if (fc.errors && fc.touched) {
      let err: string = '';
      for (const [key, val] of Object.entries(fc.errors)) {
        err = this.errorMsg[key];
      }
      return err;
    }
  }

  search(textSearch: string): void {
    if (textSearch !== '') {
      this.apiSer.searchRefferal(textSearch).subscribe((res) => {
        this.searchFc.setErrors(null);
        if (res.status && res.data && res.data.length > 0) {
          this.apiSer.setSearchData(res.data[0]);
        }
      }, (err) => {
        if (!err.error.status) {
          this.errorMsg.invalid = err.error.error_message;
          this.searchFc.setErrors({ invalid: true });
        }
      });
    }
  }
}
