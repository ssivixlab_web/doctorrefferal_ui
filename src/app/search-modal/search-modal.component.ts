import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { APIService } from '../home/home.service';

@Component({
    templateUrl: './search-modal.component.html',
    styles: [`.mat-form-field-wrapper{padding-bottom:0px;}`]
})
export class SearchModalComponent implements OnInit {
    searchDoctorFb: FormGroup;
    seachResult = [];
    isSubmitted = false;
    constructor(
        public dialogRef: MatDialogRef<SearchModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private fb: FormBuilder,
        private searApi: APIService) { }

    ngOnInit(): void {
        this.searchDoctorFb = this.fb.group({
            searchText: ['', Validators.required],
            currentLocation: [''],
            pageNo: [1],
            size: [10],
            sortFees: ['desc']
        });
    }

    doctorSelected(data, item): void {
        const paylaod = {
            ev: data,
            docObj: item
        };
        this.dialogRef.close(paylaod);
    }

    search(val): void {
        this.searApi.searchDoctorByName(val).subscribe((res) => {
            if (res && res.status && res.data.length > 0) {
                this.seachResult = res.data;
            } else {
                this.seachResult = [];
            }
        }, (err) => {
            this.seachResult = [];
        }, () => {
            this.isSubmitted = true;
        });
    }

    close(): void {
        this.dialogRef.close();
    }
}
