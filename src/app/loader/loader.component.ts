import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { LoaderService } from './loader.service';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit, OnDestroy {
  isLoading: Observable<boolean>;
  private subsciption: Subscription = new Subscription();
  public backgroundColor = '#333';
  constructor(
    private router: Router,
    private loaderService: LoaderService
  ) {
    this.subsciption.add(this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.loaderService.show();
      } else if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
        this.loaderService.hide();
      }
    }, () => {
      this.loaderService.hide();
    }));
  }

  ngOnInit() {
    this.isLoading = this.loaderService.getSpinnerStatus();
  }

  ngOnDestroy() {
    this.subsciption.unsubscribe();
  }
}
