import { environment } from 'src/environments/environment';

export const NO_INTERCEPT_LOADER = 'NO__LOADER';

export const API_END_POINT_URL = environment.endPointUrl;
export const API_URL = {
    GET_DOCTOR: 'doctor/list',
    DOCTOR_REFER: 'doctor/refer'
};
